---
title: Pass By Value And Pass By Reference In JavaScript
date: 2023-03-02 09:34:11
tags:
---

## **Introduction**

In this article, we’ll explore the concept of pass by reference and pass by value in JavaScript with code examples, as we already know about functions and various ways to invoke them.

#### **Primitive values**

Primitive values are the most basic values, which include undefined, null, boolean, string, and numbers. Primitive values are passed by value in javascript

Whereas all objects (including functions) are passed by reference in javascript.

## **Pass by value**

In Pass by value, the function is called by directly passing the value of the variable as the argument. Therefore, even changing the argument inside the function doesn’t affect the variable passed from outside the function.

It is important to note that in javascript, all function arguments are always passed by value. That is, JavaScript copies the values of the passing variables into arguments inside of the function.

``` Js
function test(passedNumber) {

    passedNumber = passedNumber * passedNumber;

    console.log("\nInside Call by Value Method");
    console.log(passedNumber);
}

let number = 10;

console.log("Before Call by Value Method");
console.log(number);

test(number);

console.log("\nAfter Call by Value Method");
console.log(number);
```

**output:**

``` Js
Before Call by Value Method
10

Inside Call by Value Method
100

After Call by Value Method
10
```

As we can see here, once we have passed the variable `number` to the function `test`, the value of `number` is copied to the variable `passedNumber` by javascript (hence pass by value).

## **Pass by Reference**

In Pass by Reference, the Function is called by directly passing the reference/address of the variable as the argument. Changing the argument inside the function affects the variable passed from outside the function. In Javascript objects and arrays follows pass by reference.

I have declared 3 different variable items, sweet1, and sweet2, and called the function on them.

``` Js
function getNewSweet(passedItems, passedSweet1, passedSweet2) {

    console.log("\nInside Call by Reference Method");

    passedItems = 5;

    passedSweet1.name = "chocolate cookie";

    passedSweet2 = {
        name: "coffee doughnut"
    };

    console.log(passedItems);
    console.log(passedSweet1.name);
    console.log(passedSweet2.name);
}

let items = 2;

let sweet1 = {
    name: "plain cookie"
};

let sweet2 = {
    name: "plain doughnut"
};

console.log("Before Call by Reference Method");

console.log(items);
console.log(sweet1.name);
console.log(sweet2.name);

getNewSweet(items, sweet1, sweet2);

console.log("\nAfter Call by Reference Method");

console.log(items);
console.log(sweet1.name);
console.log(sweet2.name);
```

**output:**

``` Js
Before Call by Reference Method
2
plain cookie
plain doughnut

Inside Call by Reference Method
5
chocolate cookie
coffee doughnut

After Call by Reference Method
2
chocolate cookie
plain doughnut
```

The primitive data types are always pass-by-values. Even if we alter the value of any primitive inside any function, the value outside the function will retain its original value and will be unaffected by the function. Additionally, objects can behave both as pass by value and pass by reference. In case the entire internal structure of an object is changed, the object behaves as pass by value. But if a parameter of the object itself is changed, it will behave as pass by reference.

Within the function, `items`, `sweet1`, and `sweet2` are references. When we change the "name" property of the object referenced by `sweet1`, we are changing the value of the "name" property that was originally set to "plain cookie". When we assign `sweet2` a value of {name: "coffee doughnut"} we are changing the reference to a new object (which immediately goes out of scope when the function exits). Thus, `sweet2` retains its original value whereas `sweet1` does not.

we can say,


- All the primitive data types in JavaScript are passed by values and the custom types such as objects and arrays are passed by references.

- Changing the value of a variable never changes the underlying primitive or object/array, it just points the variable to a new primitive or object/array.

- When a variable refers to an object or an array, the "value" contained in the variable is a reference to the object/array.

- Changing a property of an object/array referenced by a variable change the property in the referenced object/array.