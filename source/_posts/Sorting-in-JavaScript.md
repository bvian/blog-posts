---
title: Sorting in JavaScript
date: 2023-03-06 13:54:43
tags:
---

<img src=https://gitlab.com/Bhabesh16/blog-posts/-/raw/main/source/_posts/images/Array-Sort-in-Javascript.png>

## **Introduction**

In this article, we’ll explore the basic principles of the sort() array method in JavaScript, and its use cases, along with some code examples.

### **Javascript sort**

Developers often work with large amounts of data represented by different types of objects. No matter the form or size of the data collection, it will need to be sorted or organized.

Sorting is the process of organizing data into meaningful groups so that it can be easily analyzed or processed.

JavaScript, like most other programming languages, includes a built-in sort method with the following syntax:

``` Js
array.sort(compareFunction);
```

Where the array is an array identifier containing the elements we want to sort and the compareFunction is an optional function to enhance our sorting logic further. 

If a compare function is not supplied, the array elements are sorted by first converting them to strings, which are then compared and sorted in ascending order.

### **How the sort function works**

The `.sort()` method uses an algorithm called “in-place” to sort the elements in our array. This type of algorithm does not require extra disk space to sort the items but may require a small, non-constant amount of extra memory space to run. Once the sorting operation is complete, it will return the same array, now sorted, without creating a new array in the process.

### **Sort strings**

Here’s an example of utilizing sort() on an array of strings:

``` Js
const indianStates = ["New Delhi", "Bengaluru", "Mumbai", "Kolkata", "Chennai", "Hyderabad"];

indianStates.sort();
console.log(indianStates);
```

**Output**

``` Js
[ 'Bengaluru', 'Chennai', 'Hyderabad', 'Kolkata', 'Mumbai', 'New Delhi' ]
```

Here, we can see that our strings are sorted using the alphabet’s letter order.

We can also apply .`sort()` to simple integers:

``` Js
const numbers = [4, 6, 1, 3, 7, 2, 5];

numbers.sort();
console.log(numbers);
```

**Output**

``` Js
[ 1, 2, 3, 4, 5, 6, 7 ]
```

But, when we try to sort through integers with larger values, our sort function fails:

``` Js
const numbers = [4, 3, 1, 100, 300, 2, 5];

numbers.sort();
console.log(numbers);
```

**Output**

``` Js
[ 1, 100, 2, 3, 300, 4, 5 ]
```

This time `.sort` method fails because if a compare function is not provided in the `.sort()` operation, all array elements are sorted by first converting them to strings and then comparing them.

In other words, while our `.sort()` method continues to return an array of integers, we are actually attempting to sort the following array:

``` Js
[ "1", "100", "2", "3", "300", "4", "5" ]
```

And alphabetically, “100” comes before “20”. We can quickly fix this by writing our own custom compare function instead of using JavaScript’s default.

### **Use compare function to sort the data**

The compare function takes two parameters, named `compare1` and `compare2`, where `compare1` is the first element for comparison and `compare2` is the second. Because `compare2` is the second value, it is commonly referred to as the comparator. This function then runs during the `.sort()` method and assists it in determining which item in the array should come first in the results.

The compare function should be written in threeway-

- It returns a negative number if the first comparator should be sorted before the second.
- It returns 0 (zero) if the comparators should be equal or maintain the same order.
- It returns a positive number if the second comparator should be sorted before the first.

## **Sort examples using different data types**

- ### **Sort integers**

The sort method’s compare function parameter has a simple syntax; it accepts a function that takes two arguments and returns a positive, zero, or negative value. Based on the return value, it will assign a position for each item in the array. If the return value is a negative number, then the sort method knows the first number compared is lower than the second number.

Likewise, if the return value is positive, then the first number is higher, and a return value of zero implies the two values are the same.

``` Js
const numbers = [4, 3, 1, 100, 300, 2, 5];

function numbersSort(numbers) {

    const sorter = (number1, number2) => {

        if (number1 < number2) {
            return -1;
        } else if (number1 > number2) {
            return 1;
        } else {
            return 0;
        }
    }
    numbers.sort(sorter); // sorting numbers using compare function named 'sorter'
}

numbersSort(numbers);
console.log(numbers);
```

Output 

``` Js
[ 1, 2, 3, 4, 5, 100, 300 ]
```

- ### **Sort floating point numbers**

Sort floating point numbers are the same as integers.

Here also, In the compare function if the return value is a negative number, then the sort method knows the first number compared is lower than the second number.

Likewise, if the return value is positive, then the first number is higher, and a return value of zero implies the two values are the same.

``` Js
const floatNumbers = [82.11742562118049, 28.86823689842918, 49.61295450928224, 5.861613903793295, 82.11742562118046, 28.86823689842918];

function floatNumbersSort(floatNumbers) {

    const sorter = (floatNumber1, floatNumber2) => {
        return floatNumber1 - floatNumber2;
    }
    floatNumbers.sort(sorter);
}

floatNumbersSort(floatNumbers);
console.log(floatNumbers);
```

**Output**

``` Js
[ 5.861613903793295, 28.86823689842918, 28.86823689842918, 49.61295450928224, 82.11742562118046, 82.11742562118049 ]
```

- ### **Sort different cases of strings**

We have an array of string literals like this −

``` Js
const arrayStrings = ['123Bhabesh', '5 D Raj', '96 Utsav', '8Kunal', 'ankit'];

function caseSensitiveSort(arrayStrings) {

    const sorter = (string1, string2) => {
        if (string1 === string2) {      // if both string are same return 0
            return 0;
        }
        if (string1.charAt(0) === string2.charAt(0)) {      // if both string first character is same then call it again with slicing the first character 
            return sorter(string1.slice(1), string2.slice(1));
        }
        if (string1.charAt(0).toLowerCase() === string2.charAt(0).toLowerCase()) {
            if (/^[a-z]/.test(string1.charAt(0)) && /^[A-Z]/.test(string2.charAt(0))) {  // checking that charater lies between A-Z or a-z
                return -1;
            }
            if (/^[a-z]/.test(string2.charAt(0)) && /^[A-Z]/.test(string1.charAt(0))) {
                return 1;
            }
        }
        return string1.localeCompare(string2);      // comparing strings, localeCompare can also compare the Non-ASCII characters
    }
    arrayStrings.sort(sorter);
}

caseSensitiveSort(arrayStrings);
console.log(arrayStrings);
```

**Output**

``` Js
[ '123Bhabesh', '5 D Raj', '8Kunal', '96 Utsav', 'ankit' ]
```

In this, the JavaScript function takes in one array and sorts the array using "inplace".

The sorting function takes all the strings starting with special characters, numerals appear first.

Then, after that strings appear in alphabetical order, and if we have two strings starting with the same alphabet but different cases, then the string starting with the lowercase alphabet will appear first.

- ### **Sort Objects**

Arrays of objects can be sorted by comparing the value of one of their properties.

``` Js
let employeeData = [{
        name: "Raj",
        salary: 50000
    },
    {
        name: 'Utsav',
        salary: 30000
    },
    {
        name: 'Vikash',
        salary: 25000
    },
    {
        name: 'Bhabesh',
        salary: 32000,
    },
    {
        name: 'Ankit',
        salary: 35000,
    },
    {
        name: 'Kunal',
        salary: 40000,
}];

const sortBySalary = employeeData.sort((employee1, employee2) => {

    if (employee1.salary < employee2.salary) {
        return 1;
    }
    if (employee1.salary > employee2.salary) {
        return -1;
    } else {
        return 0;
    }
});

console.log(sortBySalary);
```

Output

``` Js
[
  { name: 'Raj', salary: 50000 },
  { name: 'Kunal', salary: 40000 },
  { name: 'Ankit', salary: 35000 },
  { name: 'Bhabesh', salary: 32000 },
  { name: 'Utsav', salary: 30000 },
  { name: 'Vikash', salary: 25000 }
]
```

In the above example, I have tried to sort our array based on the employee salary, in descending order. I have written a custom function, which compares two salaries and returns a positive value, a negative value, or a zero based on the salary. Likewise, we sort the complete array of objects based on this property.

- ### **Sort multiple keys in an object**

``` Js
let employeeData = [{
    name: "Raj",
    age: 26,
    salary: 50000
},
{
    name: 'Utsav',
    age: 23,
    salary: 32000
},
{
    name: 'Vikash',
    age: 25,
    salary: 30000
},
{
    name: 'Bhabesh',
    age: 23,
    salary: 32000,
},
{
    name: 'Ankit',
    age: 25,
    salary: 35000,
},
{
    name: 'Kunal',
    age: 25,
    salary: 40000,
}];

const sortByMultipleKeys = employeeData.sort((employee1, employee2) => {    // sorting based on multiple keys (i.e. salary, age, name)

    return (employee1.salary - employee2.salary || employee1.age - employee2.age || employee1.name.localeCompare(employee2.name));
});

console.log(sortByMultipleKeys);
```

**Output**

``` Js
[
  { name: 'Vikash', age: 25, salary: 30000 },
  { name: 'Bhabesh', age: 23, salary: 32000 },
  { name: 'Utsav', age: 23, salary: 32000 },
  { name: 'Ankit', age: 25, salary: 35000 },
  { name: 'Kunal', age: 25, salary: 40000 },
  { name: 'Raj', age: 26, salary: 50000 }
]
```

In the above example, I have tried to sort our array based on the employee data, in ascending order. I have written a custom function, which compares multiple keys (i.e. salary, age, and name) and returns a positive value, a negative value, or a zero based on their multiple keys comparison. Likewise, we sort the complete array of objects based on this property.

## **Conclusion**

- The array sort method is used to sort the arrays in javascript.
- By default, arrays are sorted in increasing order.
- The array sort method converts the array elements into strings before sorting.
- A function (compareFunction) can be passed to the array sort method as a parameter. This isn't a mandatory parameter.
- The compareFunction takes array elements as parameters and returns either a positive value, negative value, or zero.
- The sort() method doesn't work properly for numbers, and also on non-ASCII characters.
- The array sort method in javascript is stable.